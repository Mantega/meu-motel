package com.meumotel.enums;

public enum Erro {
	CONEXAO_INTERNET(1, "Por favor, verifique sua conex�o com a internet.");
	
	private int tipo;
	private String descricao;
	
	private Erro(int id, String descricao) {
		this.tipo = id;
		this.descricao = descricao;
	}
	
	public static Erro getDescricao(int id) {
		switch (id) {
		case 1:
			return CONEXAO_INTERNET;
		default:
			return CONEXAO_INTERNET;
		}
	}
	
	public int getTipo() {
		return tipo;
	}

	public void setTipo(int id) {
		this.tipo = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}