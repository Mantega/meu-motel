package com.meumotel.activity;

import android.app.Activity;
import android.os.Bundle;

public class PagamentoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		atribuirLayout();
		setTitle("Romanus Motel - Su�te Olimpo N�1");
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_pagamento);
	}
}
