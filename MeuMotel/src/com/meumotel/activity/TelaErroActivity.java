package com.meumotel.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.meumotel.enums.Erro;
import com.meumotel.util.Strings;

public class TelaErroActivity extends Activity implements android.view.View.OnClickListener {
	private TextView txvDescricaoErro;
	private Button btnTentarNovamente;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		atribuirLayout();
		getErro();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_erro);
		
		txvDescricaoErro = (TextView) findViewById(R.idErro.txvDescricaoErro);
		btnTentarNovamente = (Button) findViewById(R.idErro.btnTentarNovamente);
		btnTentarNovamente.setOnClickListener(this);
	}
	
	private void getErro() {
		txvDescricaoErro.setText(getIntentErro().getDescricao());
	}

	private Erro getIntentErro() {
		return (Erro) getIntent().getSerializableExtra(Strings.ERRO);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.idErro.btnTentarNovamente:
			carregarLauncher();
			break;
		}
	}
	
	private void carregarLauncher() {
		Intent it = new Intent(getBaseContext(), Launcher.class);
		it.addCategory(Strings.CATEGORIA);
		startActivity(it);
	}
}