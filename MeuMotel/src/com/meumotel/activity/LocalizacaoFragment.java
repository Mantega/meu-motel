package com.meumotel.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.librarymeumotel.mapa.Route;
import com.librarymeumotel.mapa.Routing;
import com.librarymeumotel.mapa.RoutingListener;
import com.meumotel.localizacao.ServicoLocalizacao;
import com.meumotel.modelo.Localizacao;
import com.meumotel.util.Serializacao;
import com.meumotel.util.Strings;

public class LocalizacaoFragment extends Fragment implements RoutingListener {
	MapView mMapView;
	private GoogleMap googleMap;
	protected LatLng localizacaoInicio;
    protected LatLng localizacaoDestino;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setHasOptionsMenu(true);
    }
    
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_localizacao, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.localizacao:
			localizacaoMotelMapa();
			break;
		case R.id.comoChegar:
			comoChegar();
			break;
		}
		return true;
	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = null;
		
		if(ServicoLocalizacao.existeConexaoInternet(getReservaFragmentActivity().getBaseContext())) {
			view = inflater.inflate(R.layout.fragment_localizacao, container, false);
			
			carregarMapa(savedInstanceState, view);
		} else {
			view = inflater.inflate(R.layout.fragment_localizacao_sem_conexao, container, false);
		}
		return view;
	}
	
	private void carregarMapa(Bundle savedInstanceState, View view) {
		mMapView = (MapView) view.findViewById(R.id.mapview);
		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();
		
		try {
			MapsInitializer.initialize(getReservaFragmentActivity().getApplicationContext());
			googleMap = mMapView.getMap();
			
			localizacaoMotelMapa();
		} catch(Exception e) {
			Log.e(getClass().getSimpleName() + ".onCreateView(): ", e.getMessage());
		}
	}
	
	private boolean possuiLocalizacaoUsuario(Localizacao localizacao) {
		return localizacao.getLatitude() != -1 && localizacao.getLongitude() != -1;
	}

	public void comoChegar() {
		Localizacao localizacaoUsuario = (Localizacao) Serializacao.buscarObjetoSerializado(getReservaFragmentActivity().getBaseContext(), Strings.LOCALIZACAO_SERIALIZADO);
		
		if(possuiLocalizacaoUsuario(localizacaoUsuario)) {
			definirRotaMapa(localizacaoUsuario);
		} else {
			showDialogAvisoLocalizacaoNaoEncontrada();
		}
	}
	
	private void showDialogAvisoLocalizacaoNaoEncontrada() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getReservaFragmentActivity());
		dialog.setTitle("Localiza��o n�o encontrada");
		dialog.setMessage("N�o foi poss�vel definir sua localiza��o");
		
		dialog.setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(naoPossuiServicoLocalizacaoAtivo()) {
					showDialogServicoLocalizacaoNaoAtivo();
				} else {
					tarefaBuscarLocalizacao();
				}
			}
		});
		
		dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog.create();
	}
	
	private void showDialogServicoLocalizacaoNaoAtivo() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getReservaFragmentActivity());
		dialog.setTitle("Servi�o de localiza��o n�o ativado");
		dialog.setMessage("Por favor, ative o seu servi�o de localiza��o");
		
		dialog.setPositiveButton("Ativar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				chamarTelaConfiguracao();
			}
		});
		
		dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		tarefaBuscarLocalizacao();
	}

	private void tarefaBuscarLocalizacao() {
		tarefaBuscarLocalizacao task = new tarefaBuscarLocalizacao();
		task.execute();
	}
	
	private void chamarTelaConfiguracao() {
		startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
	}
	
	private boolean naoPossuiServicoLocalizacaoAtivo() {
		return ! ServicoLocalizacao.possuiServicoLocalizacaoAtivo(getReservaFragmentActivity().getBaseContext());
	}
	
	private void localizacaoMotelMapa() {
		MarkerOptions marker = new MarkerOptions().position(new LatLng(getLocalizacaoLatitudeEmpresa(), getLocalizacaoLongitudeEmpresa())).title(getReservaFragmentActivity().getEmpresa().getNomeFantasia());
		marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
		
		CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(getLocalizacaoLatitudeEmpresa(), getLocalizacaoLongitudeEmpresa()));
		CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
		
		googleMap.moveCamera(center);
		googleMap.animateCamera(zoom);
	    googleMap.addMarker(marker);
	}

	private void definirRotaMapa(Localizacao localizacaoUsuario) {
		CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(localizacaoUsuario.getLatitude(), localizacaoUsuario.getLongitude()));
		CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
		
		googleMap.moveCamera(center);
		googleMap.animateCamera(zoom);
		
		localizacaoInicio = new LatLng(localizacaoUsuario.getLatitude(), localizacaoUsuario.getLongitude());
		localizacaoDestino = new LatLng(getLocalizacaoLatitudeEmpresa(), getLocalizacaoLongitudeEmpresa());
		
		Routing routing = new Routing(Routing.TravelMode.DRIVING);
		routing.registerListener(this);
		routing.execute(localizacaoInicio, localizacaoDestino);
	}

	private Double getLocalizacaoLatitudeEmpresa() {
		return getReservaFragmentActivity().getEmpresa().getLocalizacao().getLatitude();
	}

	private Double getLocalizacaoLongitudeEmpresa() {
		return getReservaFragmentActivity().getEmpresa().getLocalizacao().getLongitude();
	}
	
	private ReservaFragmentActivity getReservaFragmentActivity() {
		return (ReservaFragmentActivity) getActivity();
	}

	@Override
	public void onRoutingFailure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRoutingStart() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
		PolylineOptions polyoptions = new PolylineOptions();
	      polyoptions.color(Color.BLUE);
	      polyoptions.width(10);
	      polyoptions.addAll(mPolyOptions.getPoints());
	      googleMap.addPolyline(polyoptions);

	      // Start marker
	      MarkerOptions options = new MarkerOptions();
	      options.position(localizacaoInicio);
	      options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
	      googleMap.addMarker(options);

	      // End marker
	      options = new MarkerOptions();
	      options.position(localizacaoDestino);
	      options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));  
	      googleMap.addMarker(options);
	}
	
	private class tarefaBuscarLocalizacao extends AsyncTask<String, String, String> {
		private ProgressDialog pDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			carregarProgressDialog();
		}
		
		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}

		private void carregarProgressDialog() {
			pDialog = new ProgressDialog(getReservaFragmentActivity());
			pDialog.setTitle("Aguarde");
			pDialog.setMessage("Buscando localiza��o...");
			pDialog.setIndeterminate(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				buscarLocalizacao();
			} catch(Exception e) {
				return e.getMessage();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(result != null) {
				mostrarMensagem(result);
			} else {
				comoChegar();
			}
		}
		
		private void buscarLocalizacao() throws Exception {
			try {
				ServicoLocalizacao.iniciar(getReservaFragmentActivity().getBaseContext(), Strings.CATEGORIA);
			} catch(Exception e) {
				throw new Exception("Ops!, n�o foi poss�vel buscar sua localiza��o.");
			}
		}
		
		private void mostrarMensagem(String mensagem) {
			Toast.makeText(getReservaFragmentActivity().getBaseContext(), mensagem, Toast.LENGTH_LONG).show();
		}
	}
}
