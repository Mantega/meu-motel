package com.meumotel.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.meumotel.localizacao.ServicoLocalizacao;
import com.meumotel.util.Strings;

public class Launcher extends Activity implements Runnable {
	private ProgressBar pbLauncher;
	private tarefaBuscarLocalizacao task;
	private final int delay = 3000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		atribuirLayout();
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		verificarServicoLocalizacao();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(task != null) {
			task.cancel(true);
			task = null;
			finish();
		}
	}
	
	private void iniciarTarefaBuscarLocalizacao() {
		task = new tarefaBuscarLocalizacao();
		task.execute();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_launcher);
		pbLauncher = (ProgressBar) findViewById(R.idLauncher.pblauncher);
	}
	
	private void verificarServicoLocalizacao() {
		if(naoPossuiServicoLocalizacaoAtivo() && mostrarTelaAtivarServicoLocalizacao()) {
			chamarTelaAtivarServicoLocalizacao();
		} else {
			Handler h = new Handler();
			h.postDelayed(this, delay);
		}
	}
	
	private boolean mostrarTelaAtivarServicoLocalizacao() {
		SharedPreferences settings = getSharedPreferences(Strings.PREFERENCE_NOME, 0);
		return settings.getBoolean(Strings.PREFERENCE_USUARIO, true);
	}
	
	
	private boolean naoPossuiServicoLocalizacaoAtivo() {
		return ! ServicoLocalizacao.possuiServicoLocalizacaoAtivo(getBaseContext());
	}
	
	private void chamarTelaAtivarServicoLocalizacao() {
		Intent it = new Intent(Strings.ATIVAR_SERVICO_LOCALIZACAO_ACTION);
		startActivity(it);
	}

	@Override
	public void run() {
		iniciarTarefaBuscarLocalizacao();
	}
	
	private class tarefaBuscarLocalizacao extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			carregarProgressBar();
		}
		
		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
		}

		private void carregarProgressBar() {
			pbLauncher.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				buscarLocalizacao();
			} catch(Exception e) {
				return e.getMessage();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(result != null) {
				mostrarMensagem(result);
			} else {
				chamarTelaPrincipal();
			}
		}
		
		private void chamarTelaLogin() {
			Intent it = new Intent(Strings.LOGIN_ACTION);
			it.addCategory(Strings.CATEGORIA);
			startActivity(it);
		}
		
		private void chamarTelaPrincipal() {
			Intent it = new Intent(Strings.PRINCIPAL_ACTION);
			it.addCategory(Strings.CATEGORIA);
			startActivity(it);
		}
		
		private void buscarLocalizacao() throws Exception {
			try {
				ServicoLocalizacao.iniciar(getBaseContext(), Strings.CATEGORIA);
			} catch(Exception e) {
				throw new Exception("Ops!, n�o foi poss�vel buscar sua localiza��o.");
			}
		}
		
		private void mostrarMensagem(String mensagem) {
			Toast.makeText(getBaseContext(), mensagem, Toast.LENGTH_LONG).show();
		}
	}
}