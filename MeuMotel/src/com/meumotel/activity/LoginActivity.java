package com.meumotel.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.meumotel.modelo.Localizacao;
import com.meumotel.util.Serializacao;
import com.meumotel.util.Strings;

public class LoginActivity extends Activity implements OnClickListener {
	private EditText edtEmail;
	private EditText edtSenha;
	private Button btnLogar;
	private Button btnExperimentar;
	private TextView txvCadastrarNovoUsuario;
	private TextView txvLogarComFacebook;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		atribuirLayout();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_login);
		
		edtEmail = (EditText) findViewById(R.idLogin.edtEmail);
		edtSenha = (EditText) findViewById(R.idLogin.edtSenha);
		btnLogar = (Button) findViewById(R.idLogin.btnLogar);
		btnExperimentar = (Button) findViewById(R.idLogin.btnExperimentar);
		txvCadastrarNovoUsuario = (TextView) findViewById(R.idLogin.txvCadastrarNovoUsuario);
		txvLogarComFacebook = (TextView) findViewById(R.idLogin.txvLogarComFacebook);
	
		btnLogar.setOnClickListener(this);
		btnExperimentar.setOnClickListener(this);
		txvCadastrarNovoUsuario.setOnClickListener(this);
		txvLogarComFacebook.setOnClickListener(this);
	}
	
	private Localizacao getLocalizacao() {
		return (Localizacao) Serializacao.buscarObjetoSerializado(getBaseContext(), Strings.LOCALIZACAO_SERIALIZADO);
	}
	
	private void efetuarLogin() {
		Intent it = new Intent("Pagamento");
		startActivity(it);
		
		//		if(loginValido()) {
//			chamarTelaPrincipal();
//		}
	}
	
	private void chamarTelaPrincipal() {
		Intent it = new Intent("Pagamento");
		startActivity(it);
	}
	
	private boolean loginValido() {
		boolean retorno = false;
		
		if(edtEmail.getText().length() == 0) {
			setarErroNoCampo(edtEmail);
			retorno = false;
		}
		if(edtSenha.getText().length() == 0) {
			setarErroNoCampo(edtSenha);
			retorno = false;
		}
		return retorno;
	}
	
	private void setarErroNoCampo(EditText editText) {
		editText.setError("Campo obrigatório");
	}

	private void entrarSemLogar() {
		chamarTelaPrincipal();
	}

	private void efetuarLoginComFacebook() {
		
	}

	private void chamarTelaCadastroNovoUsuario() {

	
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.idLogin.btnLogar:
			efetuarLogin();
			break;
		case R.idLogin.btnExperimentar:
			entrarSemLogar();
			break;
		case R.idLogin.txvLogarComFacebook:
			efetuarLoginComFacebook();
			break;
		case R.idLogin.txvCadastrarNovoUsuario:
			chamarTelaCadastroNovoUsuario();
			break;
		}
	}
}