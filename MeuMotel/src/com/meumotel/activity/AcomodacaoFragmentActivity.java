package com.meumotel.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;

import com.librarymeumotel.pagersliding.PagerSlidingTabStrip;
import com.meumotel.adapter.AcomodacaoAdapter;
import com.meumotel.modelo.Acomodacao;

public class AcomodacaoFragmentActivity extends FragmentActivity {
	private int currentColor = Color.parseColor("#ff1f0f");
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private AcomodacaoAdapter adapter;

	public static FragmentManager fragmentManager;
	
	private Acomodacao acomodacao;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getIntentAcomodacao();
		atribuirTitulo();
		atribuirLayout();
		atribuirAdapter();
	}

	private void atribuirTitulo() {
		setTitle(String.format("%s N�%d", acomodacao.getDescricao(), acomodacao.getNumero()));
	}
	
	private void getIntentAcomodacao() {
		acomodacao = (Acomodacao) getIntent().getSerializableExtra("Acomodacao");
	}

	private void atribuirLayout() {
		setContentView(R.layout.activity_main);

		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		tabs.setIndicatorColor(currentColor);
		pager = (ViewPager) findViewById(R.id.pager);
	}
	
	private void atribuirAdapter() {
		fragmentManager = getSupportFragmentManager();
		adapter = new AcomodacaoAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
		pager.setPageMargin(pageMargin);
		tabs.setViewPager(pager);
	}
	
	public Acomodacao getAcomodacao() {
		return acomodacao;
	}
}
