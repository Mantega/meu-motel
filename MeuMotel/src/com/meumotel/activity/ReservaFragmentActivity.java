package com.meumotel.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.librarymeumotel.pagersliding.PagerSlidingTabStrip;
import com.meumotel.adapter.ReservaAdapter;
import com.meumotel.dao.DriveInDao;
import com.meumotel.dao.QuartoDao;
import com.meumotel.dao.SuiteDao;
import com.meumotel.modelo.Empresa;

public class ReservaFragmentActivity extends FragmentActivity {
	private int currentColor = Color.parseColor("#ff1f0f");
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private ReservaAdapter adapter;
	
	public static FragmentManager fragmentManager;
	
	private Empresa motel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getIntentEmpresa();
		atribuirTitulo();
		atribuirLayout();
		atribuirAdapter();
	}

	private void atribuirTitulo() {
		setTitle(motel.getNomeFantasia());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		TarefaReservaFragment tarefa = new TarefaReservaFragment();
		tarefa.execute();
	}

	private void atribuirLayout() {
		setContentView(R.layout.activity_main);

		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		tabs.setIndicatorColor(currentColor);
		pager = (ViewPager) findViewById(R.id.pager);
	}

	private void atribuirAdapter() {
		fragmentManager = getSupportFragmentManager();
		adapter = new ReservaAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
		pager.setPageMargin(pageMargin);
		tabs.setViewPager(pager);
	}
	
	private void getIntentEmpresa() {
		motel = (Empresa) getIntent().getSerializableExtra("Empresa");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
//		switch (item.getItemId()) {
//
//		case R.id.action_contact:
//			QuickContactFragment dialog = new QuickContactFragment();
//			dialog.show(getSupportFragmentManager(), "QuickContactFragment");
//			return true;
//
//		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("currentColor", currentColor);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		currentColor = savedInstanceState.getInt("currentColor");
	}
	
	public Empresa getEmpresa() {
		return motel;
	}
	
	public class TarefaReservaFragment extends AsyncTask<String, String, String> {
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			carregarProgressDialog();
			getIntentEmpresa();
		}
		
		@Override
		protected String doInBackground(String... arg0) {
			try {
				carregarAcomodacoesMotel();
			} catch(Exception e) {
				return e.getMessage();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(result != null) {
				mostrarMensagem(result);
			} else {
				fecharProgressDialog();
			}
		}
		
		private void carregarProgressDialog() {
			progressDialog = new ProgressDialog(ReservaFragmentActivity.this);
			progressDialog.setMessage("Por favor, Aguarde...");
			progressDialog.setIndeterminate(true);
			progressDialog.show();
		}
		
		private void fecharProgressDialog() {
			if(progressDialog != null && progressDialog.isShowing()) {
				progressDialog.cancel();
			}
		}
		
		private void mostrarMensagem(String mensagem) {
			Toast.makeText(getBaseContext(), mensagem, Toast.LENGTH_LONG).show();
			fecharProgressDialog();
		}
		
		private void carregarAcomodacoesMotel() {
			motel.setListaQuartos(new QuartoDao(getBaseContext()).getListaQuarto(motel.getId()));
			motel.setListaDriveIns(new DriveInDao(getBaseContext()).getListaDriveIn(motel.getId()));
			motel.setListaSuites(new SuiteDao(getBaseContext()).getListaSuite(motel.getId()));
		}
	}
}