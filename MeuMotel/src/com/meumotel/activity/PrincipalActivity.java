package com.meumotel.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;

import com.meumotel.adapter.PrincipalAdapter;
import com.meumotel.modelo.Empresa;
import com.meumotel.modelo.Localizacao;
import com.meumotel.util.ManipulacaoComponentes;
import com.meumotel.util.Serializacao;
import com.meumotel.util.Strings;

public class PrincipalActivity extends Activity implements TextWatcher, OnClickListener, OnItemClickListener {
	private PrincipalAdapter adapter;
	
	private List<Empresa> listaCompleta = new ArrayList<Empresa>();
	private List<Empresa> listaAtual = new ArrayList<Empresa>();
	private List<String> listaCidade = new ArrayList<String>();
	private List<String> listaMotel = new ArrayList<String>();
	
	private ListView ltvMoteis;
	private ImageView imvLimparFiltroCidade;
	private ImageView imvLimparFiltroMotel;
	private AutoCompleteTextView autoCompleteTxvPesquisaCidade;
	private AutoCompleteTextView autoCompleteTxvPesquisaMotel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adicionarNaLista();
		atribuirLayout();

		IniciarTarefaListarMoteis();
	}
	
	private void IniciarTarefaListarMoteis() {
		TarefaListarMoteis task = new TarefaListarMoteis();
		task.execute();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_principal);
		
		ltvMoteis = (ListView) findViewById(R.idPrincipal.ltvMoteis);
		imvLimparFiltroCidade = (ImageView) findViewById(R.idPrincipal.imvLimparFiltroCidade);
		imvLimparFiltroMotel = (ImageView) findViewById(R.idPrincipal.imvLimparFiltroMotel);
		autoCompleteTxvPesquisaCidade = (AutoCompleteTextView) findViewById(R.idPrincipal.autoCompleteTxvPesquisaCidade);
		autoCompleteTxvPesquisaMotel = (AutoCompleteTextView) findViewById(R.idPrincipal.autoCompleteTxvPesquisaMotel);
		
		autoCompleteTxvPesquisaCidade.setThreshold(1);
		autoCompleteTxvPesquisaMotel.setThreshold(1);
		
		imvLimparFiltroCidade.setOnClickListener(this);
		imvLimparFiltroMotel.setOnClickListener(this);
		autoCompleteTxvPesquisaCidade.addTextChangedListener(this);
		autoCompleteTxvPesquisaMotel.addTextChangedListener(this);
		ltvMoteis.setOnItemClickListener(this);
	}
	
	private void setAdaptersAutoCompleteTextView() {
		autoCompleteTxvPesquisaCidade.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaCidade));
		autoCompleteTxvPesquisaMotel.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaMotel));
	}
	
	private void atualizarLista() {
		listaAtual = getListaFiltrada();
		atribuirVisibilidadeBotoesLimparFiltros();
		atualizarAdapter();
	}
	
	public void atribuirVisibilidadeBotoesLimparFiltros() {
		imvLimparFiltroCidade.setVisibility(edtPesquisaCidadePreenchido() ? View.VISIBLE : View.GONE);
		imvLimparFiltroMotel.setVisibility(edtPesquisaMotelPreenchido() ? View.VISIBLE : View.GONE);
	}
	
	private List<Empresa> getListaFiltrada() {
		List<Empresa> listaFiltrada = new ArrayList<Empresa>();
		
		for(Empresa motel: listaCompleta) {
			if(edtPesquisaCidadePreenchido() && !edtPesquisaMotelPreenchido()) {
				if(motel.getEndereco().getCidade().getDescricao().startsWith(ManipulacaoComponentes.getText(autoCompleteTxvPesquisaCidade))) {
					listaFiltrada.add(motel);
				}
			} else if(!edtPesquisaCidadePreenchido() && edtPesquisaMotelPreenchido()) {
				if(motel.getNomeFantasia().startsWith(ManipulacaoComponentes.getText(autoCompleteTxvPesquisaMotel))) {
					listaFiltrada.add(motel);
				}
			} else if(edtPesquisaCidadePreenchido() && edtPesquisaMotelPreenchido()) {
				if(motel.getEndereco().getCidade().getDescricao().startsWith(ManipulacaoComponentes.getText(autoCompleteTxvPesquisaCidade)) && 
						motel.getNomeFantasia().startsWith(ManipulacaoComponentes.getText(autoCompleteTxvPesquisaMotel))) {
					listaFiltrada.add(motel);
				}
			} else {
				return listaCompleta;
			}
		}
		return listaFiltrada;
	}

	private boolean edtPesquisaMotelPreenchido() {
		return ManipulacaoComponentes.getText(autoCompleteTxvPesquisaMotel).length() > 0;
	}

	private boolean edtPesquisaCidadePreenchido() {
		return ManipulacaoComponentes.getText(autoCompleteTxvPesquisaCidade).length() > 0;
	}
	
	private void atualizarAdapter() {
		if(adapter == null) {
			adapter = new PrincipalAdapter(listaAtual, getBaseContext());
			ltvMoteis.setAdapter(adapter);
		} else {
			adapter.changeData(listaAtual, getBaseContext());
		}
	}

	private void adicionarNaLista() {
		Empresa motel = new Empresa();
		motel.setNomeFantasia("STATUS");
		motel.setId(1);
		motel.setQtdQuartosDisponiveis(10);
		motel.setQtdSuitesDisponiveis(5);
		motel.setQtdDriveInsDisponiveis(2);
		motel.getEndereco().getCidade().setDescricao("OSVALDO CRUZ - SP");
		motel.getLocalizacao().setLatitude(-21.803655);
		motel.getLocalizacao().setLongitude(-50.873548);
		
		Empresa motel2 = new Empresa();
		motel2.setNomeFantasia("SE QUE SABE");
		motel2.setId(2);
		motel2.setQtdQuartosDisponiveis(5);
		motel2.setQtdSuitesDisponiveis(5);
		motel2.setQtdDriveInsDisponiveis(2);
		motel2.getEndereco().getCidade().setDescricao("OSVALDO CRUZ - SP");
		motel2.getLocalizacao().setLatitude(-21.803655);
		motel2.getLocalizacao().setLongitude(-50.873548);

		
		Empresa motel3 = new Empresa();
		motel3.setNomeFantasia("DUBAI MOTEL");
		motel3.setId(3);
		motel3.setQtdQuartosDisponiveis(10);
		motel3.setQtdSuitesDisponiveis(6);
		motel3.setQtdDriveInsDisponiveis(4);
		motel3.getEndereco().getCidade().setDescricao("MARING� - PR");
		motel3.getLocalizacao().setLatitude(-23.417696);
		motel3.getLocalizacao().setLongitude(-51.953209);
	
		Empresa motel4 = new Empresa();
		motel4.setNomeFantasia("DALLAS MOTEL");
		motel4.setId(4);
		motel4.setQtdQuartosDisponiveis(5);
		motel4.setQtdSuitesDisponiveis(2);
		motel4.setQtdDriveInsDisponiveis(2);
		motel4.getEndereco().getCidade().setDescricao("MARING� - PR");
		motel4.getLocalizacao().setLatitude(-23.417696);
		motel4.getLocalizacao().setLongitude(-51.953209);
		
		Empresa motel5 = new Empresa();
		motel5.setNomeFantasia("ROMANUS MOTEL");
		motel5.setId(5);
		motel5.setQtdQuartosDisponiveis(15);
		motel5.setQtdSuitesDisponiveis(5);
		motel5.setQtdDriveInsDisponiveis(3);
		motel5.getEndereco().getCidade().setDescricao("MARING� - PR");
		motel5.getLocalizacao().setLatitude(-23.417696);
		motel5.getLocalizacao().setLongitude(-51.953209);
		
		Empresa motel6 = new Empresa();
		motel6.setNomeFantasia("CQ Sabe Motel");
		motel6.setId(6);
		motel6.setQtdQuartosDisponiveis(2);
		motel6.setQtdSuitesDisponiveis(1);
		motel6.setQtdDriveInsDisponiveis(9);
		motel6.getEndereco().getCidade().setDescricao("MARING� - PR");
		motel6.getLocalizacao().setLatitude(-23.417696);
		motel6.getLocalizacao().setLongitude(-51.953209);
		
		listaCompleta.add(motel);
		listaCompleta.add(motel2);
		listaCompleta.add(motel3);
		listaCompleta.add(motel4);
		listaCompleta.add(motel5);
		listaCompleta.add(motel6);
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {
		atualizarLista();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.idPrincipal.imvLimparFiltroCidade:
			autoCompleteTxvPesquisaCidade.setText("");
			break;
		case R.idPrincipal.imvLimparFiltroMotel:
			autoCompleteTxvPesquisaMotel.setText("");
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Empresa empresa = listaAtual.get(arg2);
		
		chamarTelaEmpresa(empresa);
	}

	private void carregarListas() {
		for(Empresa motel: listaCompleta) {
			listaCidade.add(motel.getEndereco().getCidade().getDescricao());
			listaMotel.add(motel.getNomeFantasia());
		}
		listaCidade = new ArrayList<String>(new HashSet<>(listaCidade));
		listaMotel = new ArrayList<String>(new HashSet<>(listaMotel));
	}
	
	private void chamarTelaEmpresa(Empresa empresa) {
		Intent it = new Intent(Strings.EMPRESA_ACTION);
		it.putExtra("Empresa", empresa);
		startActivity(it);
	}
	
	public class TarefaListarMoteis extends AsyncTask<String , String,String> {
		private String nomeCidadeLocalizacao = null;
		private ProgressDialog pgDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			carregarProgressDialog();
		}
		
		@Override
	    protected String doInBackground(String... params) {
			try {
				carregarListas();
				buscarNomeCidadePelaLocalizacao();
			} catch(Exception e) {
				return e.getMessage();
			} 
			return null;
	    }

		private void buscarNomeCidadePelaLocalizacao() throws IOException {
			try {
				Localizacao localizacao = (Localizacao) Serializacao.buscarObjetoSerializado(getBaseContext(), Strings.LOCALIZACAO_SERIALIZADO);
				
				if(possuiLocalizacaoUsuario(localizacao)) {
					Geocoder geocoder = new Geocoder(PrincipalActivity.this, Locale.getDefault());
					List<Address> addresses = geocoder.getFromLocation(localizacao.getLatitude(), localizacao.getLongitude(), 1);
					nomeCidadeLocalizacao = addresses.get(0).getAddressLine(1);
				}
			} catch(IOException e) {
				throw new IOException("Ocorreu um problema ao buscar a cidade na qual o usu�rio se encontra");
			}
		}

		private boolean possuiLocalizacaoUsuario(Localizacao localizacao) {
			return localizacao.getLatitude() != -1 && localizacao.getLongitude() != -1;
		}
		
	    @Override
	    protected void onPostExecute(String result) {
	        super.onPostExecute(result);

	        if(result != null) {
	        	imprimirMensagemErro(result);
	        } else {
	        	if(nomeCidadeLocalizacao != null) {
	        		autoCompleteTxvPesquisaCidade.setText(nomeCidadeLocalizacao.toUpperCase());
	        		autoCompleteTxvPesquisaCidade.setFocusable(false);
	        		autoCompleteTxvPesquisaCidade.setFocusableInTouchMode(true);
	        	} else {
	        		atualizarLista();
	        	}
	        }
			setAdaptersAutoCompleteTextView();
	        fecharProgressDialog();
	    }	    
	    
	    private void imprimirMensagemErro(String mensagem) {
	    	Log.e(getClass().getSimpleName() + " - ", mensagem);
	    }
	    
	    private void carregarProgressDialog() {
	    	pgDialog = new ProgressDialog(PrincipalActivity.this);
	    	pgDialog.setMessage("Buscando mot�is...");
	    	pgDialog.setIndeterminate(true);
	    	pgDialog.show();
	    }
	    
	    private void fecharProgressDialog() {
    		pgDialog.dismiss();
	    }
	}
}