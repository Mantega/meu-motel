package com.meumotel.activity;

import android.app.Activity;
import android.os.Bundle;

public class RetornoPedidoReservaActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Romanus Motel - Su�te Olimpo N�1");
		atribuirLayout();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_retorno_pedido_reserva);
	}
}
