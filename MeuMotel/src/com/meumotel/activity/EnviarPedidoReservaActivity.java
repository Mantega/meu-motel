package com.meumotel.activity;

import android.app.Activity;
import android.os.Bundle;

public class EnviarPedidoReservaActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle("Romanus Motel - Su�te Olimpo N�1");
		atribuirLayout();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_confirmacao_reserva);
	}
}
