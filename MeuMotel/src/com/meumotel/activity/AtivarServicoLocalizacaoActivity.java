package com.meumotel.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

import com.meumotel.util.Strings;

public class AtivarServicoLocalizacaoActivity extends Activity implements OnClickListener {
	private Button btnQueroAtivar;
	private Button btnAgoraNao;
	private CheckBox cbNaoPerguntarNovamente;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		atribuirLayout();
	}
	
	private void atribuirLayout() {
		setContentView(R.layout.activity_ativar_servico_localizacao);
		
		btnQueroAtivar = (Button) findViewById(R.idDialogAtivarServLoc.btnQueroAtivar);
		btnAgoraNao = (Button) findViewById(R.idDialogAtivarServLoc.btnAgoraNao);
		cbNaoPerguntarNovamente = (CheckBox) findViewById(R.idDialogAtivarServLoc.cbNaoPerguntarNovamente);
		
		btnAgoraNao.setOnClickListener(this);
		btnQueroAtivar.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.idDialogAtivarServLoc.btnQueroAtivar:
			chamarTelaConfiguracao();
			break;
			
		case R.idDialogAtivarServLoc.btnAgoraNao:
			chamarTelaPrincipal();
			verificarMostrarTelaNovamente();
			break;
		default:
			break;
		}
	}
	
	private void chamarTelaPrincipal() {
		Intent it = new Intent(Strings.PRINCIPAL_ACTION);
		it.addCategory(Strings.CATEGORIA);
		startActivity(it);
	}
	
	private void chamarTelaConfiguracao() {
		startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
	}
	
	private void chamarTelaLogin() {
		Intent it = new Intent(Strings.LOGIN_ACTION);
		startActivity(it);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		chamarTelaLauncher();
		verificarMostrarTelaNovamente();
	}

	private void verificarMostrarTelaNovamente() {
		if(cbNaoPerguntarNovamente.isChecked()) {
			salvarOpcaoUsuario();
		}
	}
	
	private void salvarOpcaoUsuario() {
		SharedPreferences settings = getSharedPreferences(Strings.PREFERENCE_NOME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(Strings.PREFERENCE_USUARIO, false);
		
		editor.commit();
	}
	
	private void chamarTelaLauncher() {
		Intent it = new Intent(getBaseContext(), Launcher.class);
		startActivity(it);
	}
}