package com.meumotel.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.adapter.QuartoFragmentAdapter;
import com.meumotel.modelo.Acomodacao;
import com.meumotel.util.Strings;

public class QuartoFragment extends Fragment implements OnItemClickListener {
	private ListView ltvQuartos;
	private QuartoFragmentAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		atualizarAdapter();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_quarto, container, false);
		
		ltvQuartos = (ListView) view.findViewById(R.idQuartoFragment.ltvQuarto);
		ltvQuartos.setOnItemClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	private void atualizarAdapter() {
		if(adapter == null) {
			adapter = new QuartoFragmentAdapter(getReservaFragmentActivity());
			ltvQuartos.setAdapter(adapter);
		} else {
			adapter.change(getReservaFragmentActivity());
			ltvQuartos.setAdapter(adapter);
		}
	}
	
	private ReservaFragmentActivity getReservaFragmentActivity() {
		return (ReservaFragmentActivity) getActivity();
	}

	private void abrirAcomodacaoReserva(Acomodacao quarto) {
		Intent it = new Intent(Strings.ACOMODACAO_RESERVA_ACTION);
		it.addCategory(Strings.CATEGORIA);
		it.putExtra("Acomodacao", quarto);
		startActivity(it);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Acomodacao acomodacao = getReservaFragmentActivity().getEmpresa().getListaQuartos().get(position);
		abrirAcomodacaoReserva(acomodacao);
	}
}