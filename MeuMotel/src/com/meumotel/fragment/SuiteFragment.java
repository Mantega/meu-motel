package com.meumotel.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.adapter.SuiteFragmentAdapter;
import com.meumotel.modelo.Acomodacao;
import com.meumotel.util.Strings;

public class SuiteFragment extends Fragment implements OnItemClickListener {
	private ListView ltvSuites;
	private SuiteFragmentAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view  = inflater.inflate(R.layout.fragment_suite, container, false);
		
		ltvSuites = (ListView) view.findViewById(R.idSuiteFragment.ltvSuite);
		ltvSuites.setOnItemClickListener(this);
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		atualizarAdapter();
	}
	
	private void atualizarAdapter() {
		if(adapter == null) {
			adapter = new SuiteFragmentAdapter(getReservaFragmentActivity());
			ltvSuites.setAdapter(adapter);
		} else {
			adapter.change(getReservaFragmentActivity());
			ltvSuites.setAdapter(adapter);
		}
	}
	
	private ReservaFragmentActivity getReservaFragmentActivity() {
		return (ReservaFragmentActivity) getActivity();
	}
	
	private void abrirAcomodacaoReserva(Acomodacao suite) {
		Intent it = new Intent(Strings.ACOMODACAO_RESERVA_ACTION);
		it.addCategory(Strings.CATEGORIA);
		it.putExtra("Acomodacao", suite);
		startActivity(it);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Acomodacao acomodacao = getReservaFragmentActivity().getEmpresa().getListaSuites().get(position);
		abrirAcomodacaoReserva(acomodacao);
	}
}
