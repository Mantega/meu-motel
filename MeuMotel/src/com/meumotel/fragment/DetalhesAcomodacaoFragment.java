package com.meumotel.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.meumotel.activity.AcomodacaoFragmentActivity;
import com.meumotel.activity.R;
import com.meumotel.adapter.DetalhesAcomodacaoFragmentAdapter;

public class DetalhesAcomodacaoFragment extends Fragment {
	private ExpandableListView expandableListView;
	private DetalhesAcomodacaoFragmentAdapter adapter;
	private TextView txvTituloAcomodacao;
	private ImageView imvAcomodacao;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_detalhes_acomodacao, container, false);
		
		expandableListView = (ExpandableListView) view.findViewById(R.idDetalhesAcomodacaoFragment.expandableLtvDetalhesAcomodacao);
		txvTituloAcomodacao = (TextView) view.findViewById(R.idDetalhesAcomodacaoFragment.txvTituloAcomodacao);
		imvAcomodacao = (ImageView) view.findViewById(R.idDetalhesAcomodacaoFragment.imvAcomodacao);
		
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		
		iniciarComponentes();
		atualizarAdapter();
	}
	
	private void iniciarComponentes() {
		txvTituloAcomodacao.setText(getAcomodacaoFragmentActivity().getAcomodacao().getDescricao());
	}

	private void atualizarAdapter() {
		if(adapter == null) {
			adapter = new DetalhesAcomodacaoFragmentAdapter(DetalhesAcomodacaoFragment.this);
			expandableListView.setAdapter(adapter);
		}
	}
	
	public AcomodacaoFragmentActivity getAcomodacaoFragmentActivity() {
		return (AcomodacaoFragmentActivity) getActivity();
	}
}
