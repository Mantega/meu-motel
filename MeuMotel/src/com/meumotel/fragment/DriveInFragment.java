package com.meumotel.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.adapter.DriveInFragmentAdapter;
import com.meumotel.modelo.Acomodacao;
import com.meumotel.util.Strings;

public class DriveInFragment extends Fragment implements OnItemClickListener {
	private ListView ltvDriveIns;
	private DriveInFragmentAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_drivein, container, false);
		
		ltvDriveIns = (ListView) view.findViewById(R.idDriveInFragment.ltvDriveIn);
		ltvDriveIns.setOnItemClickListener(this);
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		atualizarAdapter();
	}
	
	private void atualizarAdapter() {
		if(adapter == null) {
			adapter = new DriveInFragmentAdapter(getReservaFragmentActivity());
			ltvDriveIns.setAdapter(adapter);
		} else {
			adapter.change(getReservaFragmentActivity());
			ltvDriveIns.setAdapter(adapter);
		}
	}
	
	private ReservaFragmentActivity getReservaFragmentActivity() {
		return (ReservaFragmentActivity) getActivity();
	}
	
	private void abrirAcomodacaoReserva(Acomodacao driveIn) {
		Intent it = new Intent(Strings.ACOMODACAO_RESERVA_ACTION);
		it.addCategory(Strings.CATEGORIA);
		it.putExtra("Acomodacao", driveIn);
		startActivity(it);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		Acomodacao acomodacao = getReservaFragmentActivity().getEmpresa().getListaDriveIns().get(position);
		abrirAcomodacaoReserva(acomodacao);
	}
}