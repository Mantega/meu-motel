package com.meumotel.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.meumotel.activity.AcomodacaoFragmentActivity;
import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.excecao.HoraInvalidaException;
import com.meumotel.modelo.Acomodacao;

public class SolicitacaoReservaAcomodacaoFragment extends Fragment implements OnClickListener, OnTouchListener {
	private TextView txvTotalHoras;
	private TextView txvTotalBruto;
	private EditText edtDataReserva;
	
	private TimePicker tpHoraInicialReserva;
	private TimePicker tpHoraFinalReserva;
	
	private ImageView imvCancelar;
	private ImageView imvConfirmar;
	
	private Acomodacao acomodacao;
	
	private int mAno, mMes, mDia;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_acomodacao_reserva, container, false);
	
		edtDataReserva = (EditText) view.findViewById(R.idAcomodacaoReserva.edtDataReserva);
		txvTotalBruto = (TextView) view.findViewById(R.idAcomodacaoReserva.txvTotalBruto);
		txvTotalHoras = (TextView) view.findViewById(R.idAcomodacaoReserva.txvTotalHoras);
		imvCancelar = (ImageView) view.findViewById(R.idAcomodacaoReserva.imvCancelar);
		imvConfirmar = (ImageView) view.findViewById(R.idAcomodacaoReserva.imvConfirmar);
		tpHoraInicialReserva = (TimePicker) view.findViewById(R.idAcomodacaoReserva.tpHoraInicioReserva);
		tpHoraFinalReserva = (TimePicker) view.findViewById(R.idAcomodacaoReserva.tpHoraFimReserva);
		
		tpHoraInicialReserva.setIs24HourView(true);
		tpHoraFinalReserva.setIs24HourView(true);
		edtDataReserva.setOnTouchListener(this);
		imvCancelar.setOnClickListener(this);
		imvConfirmar.setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();

		getIntentAcomodacao();
		atribuirValores();
	}
	
	private void atribuirValores() {
		Calendar c = Calendar.getInstance();
		
		try {
			edtDataReserva.setText(sdf.format(c.getTime()));
			tpHoraInicialReserva.setCurrentHour(c.get(Calendar.HOUR_OF_DAY) + 1);
			tpHoraFinalReserva.setCurrentHour(tpHoraInicialReserva.getCurrentHour() + 1);
		} catch(IllegalArgumentException e) {
			tpHoraInicialReserva.setCurrentHour(0);
			tpHoraFinalReserva.setCurrentHour(1);
		}
	}
	
	private void getIntentAcomodacao() {
		acomodacao = (Acomodacao) getReservaFragmentActivity().getIntent().getSerializableExtra("Acomodacao");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.idAcomodacaoReserva.imvCancelar:
			getReservaFragmentActivity().finish();
			break;
		case R.idAcomodacaoReserva.imvConfirmar:
			confirmarReserva();
			break;
		}
	}

	private void confirmarReserva() {
		try {
			validarHoraReserva();
			validarDataReserva();
			abrirTelaEnviarPedidoReserva();
		} catch(HoraInvalidaException e) {
			Toast.makeText(getReservaFragmentActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	public void validarHoraReserva() throws HoraInvalidaException {
//		if(tpHoraFinalReserva.getCurrentHour() < tpHoraInicialReserva.getCurrentHour()) {
//			throw new HoraInvalidaException("A hora final n�o pode ser menor que a hora inicial");
//		}
	}
	
	private void validarDataReserva() {
		
	}
	
	private void abrirTelaEnviarPedidoReserva() {
		Intent it = new Intent("Login");
		startActivity(it);
	}
	
	private void showDialogDataReserva() {
		final Calendar c = Calendar.getInstance();
		mAno = c.get(Calendar.YEAR);
		mMes = c.get(Calendar.MONTH);
		mDia = c.get(Calendar.DAY_OF_MONTH);
		 
		DatePickerDialog dpd = new DatePickerDialog(getReservaFragmentActivity(),
		        new DatePickerDialog.OnDateSetListener() {
		 
		            @Override
		            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		            	String data = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
		            	Date dataEscolhida = new Date();
		            	try {
							dataEscolhida = sdf.parse(data);
							if(dataEscolhida.before(dataHoje())) {
								Toast.makeText(getReservaFragmentActivity().getBaseContext(), "A data escolhida tem que ser maior ou igual a data atual", Toast.LENGTH_LONG).show();
							} else {
								edtDataReserva.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
							}
						} catch (ParseException e) {
							Log.e(getClass().getSimpleName() + ".showDialogDataReserva(): ", e.getMessage());
						}
		            }
		        }, mAno, mMes, mDia);
		dpd.show();
	}

	private Date dataHoje() throws ParseException {
		Date dataHoje = new Date();
		String data = sdf.format(dataHoje);
		dataHoje = sdf.parse(data);
		
		return dataHoje;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(event.getAction() ==  MotionEvent.ACTION_DOWN) {
			showDialogDataReserva();
		}
		return false;
	}
	
	private AcomodacaoFragmentActivity getReservaFragmentActivity() {
		return (AcomodacaoFragmentActivity) getActivity();
	}
}