package com.meumotel.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.meumotel.modelo.Banco;

public class DbHelper extends SQLiteOpenHelper {
	
	public DbHelper(Context context) {
		super(context, Banco.nome , null, Banco.versao);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		
	}
}
