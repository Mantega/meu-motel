package com.meumotel.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;
import android.util.Log;

public class Serializacao {

	
	public static void serializarObjeto(Context ctx, Object obj, String nomeArquivo) {
		try {
			FileOutputStream fos = ctx.openFileOutput(nomeArquivo, Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(obj);
			os.close();
			os.flush();
			fos.close();
			fos.flush();
		} catch(Exception e) {
			Log.e("Serializacao.serializarObjeto()", e.getMessage());
		}
	}
	
	public static Object buscarObjetoSerializado(Context ctx, String nomeArquivo) {
		Object retorno = null;
		try {
			FileInputStream fis = ctx.openFileInput(nomeArquivo);
			ObjectInputStream is = new ObjectInputStream(fis);
			retorno =  is.readObject();
			is.close();
		} catch(Exception e) {
			Log.e("Serializacao.buscarObjetoSerializado()", e.getMessage());
		}
		return retorno;
	}
}
