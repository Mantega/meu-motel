package com.meumotel.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.meumotel.modelo.Foto;

public class ViewPagerActivity extends Activity {
	private static final String EXTRA_ID_PRODUTO = "idProduto";
	private static final String ACTION = "VisualizadorFotosProduto";

	private ViewPager mViewPager;
	private ArrayList<Foto> mListaImagens;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		bindExtras();
//		mViewPager = new HackyViewPager(this);

		PagerTitleStrip titulo = new PagerTitleStrip(mViewPager.getContext());
		titulo.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		titulo.setBackgroundColor(Color.WHITE);
		titulo.setTextColor(Color.BLACK);
		titulo.setGravity(Gravity.TOP);

		mViewPager.addView(titulo);

		setContentView(mViewPager);
		mViewPager.setAdapter(new SamplePagerAdapter(mListaImagens));
	}

	private void bindExtras() {
		String idProduto = getIntent().getExtras().getString(EXTRA_ID_PRODUTO); 
//		mListaImagens = new FotoDao(getBaseContext()).getFotosProduto(idProduto); 

		if(mListaImagens == null || mListaImagens.isEmpty()) {
			Toast.makeText(getBaseContext(), "N�o foi poss�vel localizar as imagens necess�rias . . .", Toast.LENGTH_LONG).show();
			finish();
		}
	}

	public static void iniciar(Activity activity, String idProduto) throws Exception {
		if(idProduto == null || idProduto.length() == 0)
			throw new Exception("N�o foi poss�vel localizar o c�digo do produto!");

		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_ID_PRODUTO, idProduto);

		Intent intent = new Intent(ACTION);
		intent.putExtras(bundle);
		activity.startActivity(intent);
	}

	static class SamplePagerAdapter extends PagerAdapter {
		private ArrayList<Foto> mListaImagens;

		public SamplePagerAdapter(ArrayList<Foto> listaImagens) {
			this.mListaImagens = listaImagens;
		}

		@Override
		public int getCount() {
			return mListaImagens.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return String.format("Foto %d de %d", position + 1, getCount());
		}

		@Override
		public View instantiateItem(ViewGroup container, final int position) {
//			Foto foto = mListaImagens.get(position);
//
////			PhotoView photoView = new PhotoView(container.getContext());
////			photoView.setImageBitmap(foto.getBitmapImagem());
////			photoView.setPadding(0,30,0,0);
//
//			container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//
//			return photoView;
			return null;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}
}