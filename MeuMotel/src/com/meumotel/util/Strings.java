package com.meumotel.util;

public class Strings {
	public final static String URL = "url";
	public final static String ERRO = "erro";
	public final static String CATEGORIA = "MeuMotel";
	public final static String EMPRESA_ACTION = "Empresa";
	public final static String ACOMODACAO_RESERVA_ACTION = "AcomodacaoReserva";
	public final static String LAUNCHER_ACTION = "Launcher";
	public final static String PRINCIPAL_ACTION = "Principal";
	public final static String ATIVAR_SERVICO_LOCALIZACAO_ACTION = "AtivarServicoLocalizacao";
	public final static String ERRO_ACTION = "TelaErro";
	public final static String LOGIN_ACTION = "Login";
	public final static String LOCALIZACAO_SERIALIZADO = "localizacao.srl";
	public final static String LOCALIZACAO = "Localizacao";
	public final static String PREFERENCE_NOME = "Preferences";
	public final static String PREFERENCE_USUARIO = "PrefUsuario";
}
