package com.meumotel.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.modelo.Quarto;

public class QuartoFragmentAdapter extends BaseAdapter {
	private List<Quarto> lista;
	private Context context;
	
	public QuartoFragmentAdapter(ReservaFragmentActivity reservaFragmentActivity) {
		this.lista = reservaFragmentActivity.getEmpresa().getListaQuartos();
		this.context = reservaFragmentActivity.getBaseContext();
	}
	
	public void change(ReservaFragmentActivity reservaFragmentActivity) {
		this.lista = reservaFragmentActivity.getEmpresa().getListaQuartos();
		this.context = reservaFragmentActivity.getBaseContext();
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Quarto quarto = lista.get(position);
		
		if(view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.linha_quarto_fragment, parent, false);
		}
		
		TextView txvNumeroQuarto = (TextView) view.findViewById(R.idLinhaQuartoFragment.txvNumeroQuarto);
		txvNumeroQuarto.setText(String.format("%s N� %d", quarto.getDescricao(), quarto.getNumero()));
		
		ImageView imvStatusQuarto = (ImageView) view.findViewById(R.idLinhaQuartoFragment.imvStatusQuarto);
		imvStatusQuarto.setImageResource(quarto.isStatusDisponivel() ? R.drawable.icone_disponivel : R.drawable.icone_ocupado);
		
		return view;
	}
}