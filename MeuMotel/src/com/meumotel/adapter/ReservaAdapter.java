package com.meumotel.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.meumotel.activity.LocalizacaoFragment;
import com.meumotel.fragment.DriveInFragment;
import com.meumotel.fragment.QuartoFragment;
import com.meumotel.fragment.SuiteFragment;

public class ReservaAdapter extends FragmentPagerAdapter {
	private static final int QUARTO_FRAGMENT = 0;
	private static final int SUITE_FRAGMENT = 1;
	private static final int DRIVEIN_FRAGMENT = 2;
	private static final int LOCALIZACAO_FRAGMENT = 3;
	
	private final String[] TITLES = { "Quarto", "Su�te", "Drive-in", "Localiza��o" };

	public ReservaAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position];
	}

	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case QUARTO_FRAGMENT:
			return new QuartoFragment();
		case SUITE_FRAGMENT:
			return new SuiteFragment();
		case DRIVEIN_FRAGMENT:
			return new DriveInFragment();
		case LOCALIZACAO_FRAGMENT:
			return new LocalizacaoFragment();
		default:
			return new LocalizacaoFragment();
		}
	}
}