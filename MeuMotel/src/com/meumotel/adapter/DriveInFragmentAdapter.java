package com.meumotel.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.modelo.DriveIn;

public class DriveInFragmentAdapter extends BaseAdapter {
	private List<DriveIn> lista;
	private Context context;
	
	public DriveInFragmentAdapter(ReservaFragmentActivity reservaFragmentActivity) {
		this.lista = reservaFragmentActivity.getEmpresa().getListaDriveIns();
		this.context = reservaFragmentActivity.getBaseContext();
	}
	
	public void change(ReservaFragmentActivity reservaFragmentActivity) {
		this.lista = reservaFragmentActivity.getEmpresa().getListaDriveIns();
		this.context = reservaFragmentActivity.getBaseContext();
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		DriveIn driveIn = lista.get(position);
		
		if(view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.linha_drivein_fragment, parent, false); 
		}
		
		TextView txvNumeroDriveIn = (TextView) view.findViewById(R.idLinhaDriveInFragment.txvNumeroDriveIn);
		txvNumeroDriveIn.setText(String.format("Drive-In N� %d", driveIn.getNumero()));
		
		ImageView imvStatusDriveIn = (ImageView) view.findViewById(R.idLinhaDriveInFragment.imvStatusDriveIn);
		imvStatusDriveIn.setImageResource(driveIn.isStatusDisponivel() ? R.drawable.icone_disponivel : R.drawable.icone_ocupado);
		
		return view;
	}
}