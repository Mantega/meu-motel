package com.meumotel.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meumotel.activity.R;
import com.meumotel.activity.ReservaFragmentActivity;
import com.meumotel.modelo.Suite;

public class SuiteFragmentAdapter extends BaseAdapter {
	private List<Suite> lista;
	private Context context;

	public SuiteFragmentAdapter(ReservaFragmentActivity reservaFragmentActivity) {
		this.lista = reservaFragmentActivity.getEmpresa().getListaSuites();
		this.context = reservaFragmentActivity.getBaseContext();
	}
	
	public void change(ReservaFragmentActivity reservaFragmentActivity) {
		this.lista = reservaFragmentActivity.getEmpresa().getListaSuites();
		this.context = reservaFragmentActivity.getBaseContext();
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Suite suite = lista.get(position);
		
		if(view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.linha_suite_fragment, parent, false);		
		}
			
		TextView txvNumeroSuite = (TextView) view.findViewById(R.idLinhaSuiteFragment.txvNumeroSuite);
		txvNumeroSuite.setText(String.format("%s N� %d", suite.getDescricao(), suite.getNumero()));
		
		ImageView imvStatusSuite = (ImageView) view.findViewById(R.idLinhaSuiteFragment.imvStatusSuite);
		imvStatusSuite.setImageResource(suite.isStatusDisponivel() ? R.drawable.icone_disponivel : R.drawable.icone_ocupado);
		
		return view;
	}
}