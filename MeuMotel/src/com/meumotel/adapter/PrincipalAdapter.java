package com.meumotel.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.meumotel.activity.R;
import com.meumotel.modelo.Empresa;

public class PrincipalAdapter extends BaseAdapter {
	private List<Empresa> lista;
	private Context context;
	
	public PrincipalAdapter(List<Empresa> lista, Context ctx) {
		this.lista = lista;
		this.context = ctx;
	}
	
	public void changeData(List<Empresa> lista , Context ctx) {
		this.lista = lista;
		this.context = ctx;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View pView, ViewGroup parent) {
		Empresa motel = lista.get(position);
		
		if(pView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			pView = inflater.inflate(R.layout.linha_principal_activity, parent, false);
		}

		ImageView imvLogoMotel = (ImageView) pView.findViewById(R.idLinhaPrincipal.imvLogoMotel);
	    
	    TextView txvTituloNomeMotel = (TextView) pView.findViewById(R.idLinhaPrincipal.txvTituloNomeMotel);
	    txvTituloNomeMotel.setText(motel.getNomeFantasia());
	    
	    TextView txvQtdQuartosDisponiveis = (TextView) pView.findViewById(R.idLinhaPrincipal.txvQtdQuartosDisponiveis);
	    txvQtdQuartosDisponiveis.setText(String.format("Quartos disp: %d", motel.getQtdQuartosDisponiveis()));
	    
	    TextView txvQtdSuitesDisponiveis = (TextView) pView.findViewById(R.idLinhaPrincipal.txvQtdSuitesDisponiveis);
	    txvQtdSuitesDisponiveis.setText(String.format("Su�tes disp: %d", motel.getQtdSuitesDisponiveis()));
	    
	    TextView txvQtdDriveInDisponiveis = (TextView) pView.findViewById(R.idLinhaPrincipal.txvQtdDriveInDisponiveis);
	    txvQtdDriveInDisponiveis.setText(String.format("Drive-in disp: %d", motel.getQtdDriveInsDisponiveis()));
		
		return pView;
	}
}