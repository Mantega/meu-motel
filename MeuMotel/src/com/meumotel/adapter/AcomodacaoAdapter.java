package com.meumotel.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.meumotel.activity.LocalizacaoFragment;
import com.meumotel.fragment.DetalhesAcomodacaoFragment;
import com.meumotel.fragment.SolicitacaoReservaAcomodacaoFragment;

public class AcomodacaoAdapter extends FragmentPagerAdapter {
	private static final int DETALHES_ACOMODACAO_FRAGMENT = 0;
	private static final int SOLICITAR_RESERVA_ACOMODACAO_FRAGMENT = 1;
	
	private final String[] TITLES = { "Detalhes", "Solicitar Reserva" };

	public AcomodacaoAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position];
	}

	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case DETALHES_ACOMODACAO_FRAGMENT:
			return new DetalhesAcomodacaoFragment();
		case SOLICITAR_RESERVA_ACOMODACAO_FRAGMENT:
			return new SolicitacaoReservaAcomodacaoFragment();
		default:
			return new LocalizacaoFragment();
		}
	}
}