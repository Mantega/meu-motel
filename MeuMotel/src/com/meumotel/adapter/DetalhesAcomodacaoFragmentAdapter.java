package com.meumotel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.meumotel.activity.R;
import com.meumotel.fragment.DetalhesAcomodacaoFragment;

public class DetalhesAcomodacaoFragmentAdapter extends BaseExpandableListAdapter {
	private Context ctx;
	private DetalhesAcomodacaoFragment detalhesAcomodacaoFragment;
	
	String[] listaPai = { "Itens", "Pre�os" };
	String[][] listaFilho = { {"Ar-condicionado Split", "TV LCD 32", "10 canais de som", "secador de cabelo"}, {"Sexta a Domingo. Per�odo 02 horas R$ 81,00", "Pernoite 12 horas com Caf� R$ 125,00"}};
	
	public DetalhesAcomodacaoFragmentAdapter(DetalhesAcomodacaoFragment detalhesAcomodacaoFragment) {
		this.ctx = detalhesAcomodacaoFragment.getAcomodacaoFragmentActivity().getBaseContext();
		this.detalhesAcomodacaoFragment = detalhesAcomodacaoFragment;
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return listaFilho[groupPosition][childPosition];
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if(v == null) {
			v = getLayoutInflater().inflate(R.layout.child_adapter_detalhes, parent, false); 
		}
		
		TextView txvDetalhes = (TextView) v.findViewById(R.idChildDetalhes.txvTitulo);
		txvDetalhes.setText(listaFilho[groupPosition][childPosition]);
		
		return v;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return listaFilho[groupPosition].length;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return listaPai[groupPosition];
	}

	@Override
	public int getGroupCount() {
		return listaPai.length;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean childPosition, View convertView, ViewGroup parent) {
		View view = convertView;
		
		if(view == null) {
			view = getLayoutInflater().inflate(R.layout.group_adapter_titulo, parent, false);
		}
		
		TextView txvTitulo = (TextView) view.findViewById(R.idGroupTitulo.txvTitulo);
		txvTitulo.setText(listaPai[groupPosition]);
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		return false;
	}
	
	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
}