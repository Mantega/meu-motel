package com.meumotel.localizacao;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.meumotel.modelo.Localizacao;
import com.meumotel.util.Serializacao;
import com.meumotel.util.Strings;

public class ServicoLocalizacao extends Service implements LocationListener {
	private Localizacao localizacao;
	private double latitude, longitude;
	private LocationManager _lManager;
	boolean _gpsEnabled;
	private Location location;
	private boolean isGPSEnabled = false;
	private boolean isNetworkEnabled = false;
	
	public static final String GPS_PROVIDER = LocationManager.GPS_PROVIDER;
	public static final String NETWORK_PROVIDER = LocationManager.NETWORK_PROVIDER;
	
	 private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
	 private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
	 
	
	@Override
	public void onCreate() {
		super.onCreate();
		getLocalizacao();
	}
	
	private void getLocalizacao() {
		try {
			_lManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
			_lManager.getProviders(true);

			isGPSEnabled = _lManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	        isNetworkEnabled = _lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	
            if (isNetworkEnabled) {
            	buscarLocalizacaoInternet();
            }
            if (isGPSEnabled) {
                buscarLocalizacaoGps();
            }
		} catch (Exception e) {
			Log.e("Erro ao buscar a localização: ", e.getMessage());
			stopSelf();
		}
	}

	private void buscarLocalizacaoGps() throws Exception {
		if (location == null) {
			_lManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
		    if (_lManager != null) {
		        location = _lManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		        if (location != null) {
		            latitude = location.getLatitude();
		            longitude = location.getLongitude();
		            atribuirLocalizacao(latitude, longitude);
		        } else {
		        	atribuirLocalizacao(-1, -1);
		        }
		    }
		}
	}

	private void buscarLocalizacaoInternet() throws Exception {
		_lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
		if (_lManager != null) {
		    location = _lManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		    if (location != null) {
		        latitude = location.getLatitude();
		        longitude = location.getLongitude();
		        atribuirLocalizacao(latitude, longitude);
		    } else {
		    	atribuirLocalizacao(-1, -1);
		    }
		}
	}

	private void atribuirLocalizacao(double latitude, double longitude) throws Exception {
		localizacao = new Localizacao();
		localizacao.setLongitude(longitude);
		localizacao.setLatitude(latitude);
		
		try {
			Serializacao.serializarObjeto(getBaseContext(), localizacao, Strings.LOCALIZACAO_SERIALIZADO);
		} catch(Exception e) {
			throw new Exception("Erro ao serializar a localização");
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("Service", "service -  destroindo serviço");
		stopSelf();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public static void iniciar(Context ctx, String categoria) {
		Intent it = new Intent(ctx, ServicoLocalizacao.class);
		it.addCategory(categoria);								
		ctx.startService(it);	
	}
	
	public static boolean existeProvedorMapeamentoAtivo(final Context context) {
		LocationManager lManager = 	(LocationManager) context.getSystemService(ServicoLocalizacao.LOCATION_SERVICE);
		if(lManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
			return true ;		
		
		if(lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && existeConexaoInternet(context)) 
			return true ;
		
		return false ;
	}

	public static boolean existeConexaoInternet(Context context) {
		boolean retorno = false;

		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conManager.getActiveNetworkInfo() != null && conManager.getActiveNetworkInfo().isConnectedOrConnecting() && conManager.getActiveNetworkInfo().isAvailable()) {
			retorno = true;
		}
		return retorno;
	}
	
	public static boolean possuiServicoLocalizacaoAtivo(Context ctx) {
		LocationManager lManager;
		boolean gpsAtivo;
		boolean netWorkAtivo;
		
		lManager = (LocationManager) ctx.getSystemService(LOCATION_SERVICE);
		lManager.getProviders(true);

		gpsAtivo = lManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        netWorkAtivo = lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        
		return !gpsAtivo && !netWorkAtivo ? false : true;
	}

	@Override
	public void onLocationChanged(Location location) {}

	@Override
	public void onProviderDisabled(String provider) {}

	@Override
	public void onProviderEnabled(String provider) {}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}
}