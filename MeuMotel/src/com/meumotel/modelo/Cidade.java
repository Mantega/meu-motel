package com.meumotel.modelo;

import java.io.Serializable;

public class Cidade implements Serializable {
	private static final long serialVersionUID = -6913314495844482311L;

	private Integer id;
	private String descricao;
	private Uf uf;

	public Cidade() {
		uf = new Uf();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}