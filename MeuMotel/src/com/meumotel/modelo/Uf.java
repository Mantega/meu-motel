package com.meumotel.modelo;

import java.io.Serializable;

public class Uf implements Serializable {
	private static final long serialVersionUID = 5262523899372445667L;

	private String descricao;
	private String sigla;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
