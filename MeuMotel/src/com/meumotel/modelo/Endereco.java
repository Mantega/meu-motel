package com.meumotel.modelo;

import java.io.Serializable;

public class Endereco implements Serializable {
	private static final long serialVersionUID = 3286229993793024499L;

	private Integer id;
	private String rua;
	private String bairro;
	private String numero;
	private String complemento;
	private Cidade cidade;

	public Endereco() {
		cidade = new Cidade();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
}