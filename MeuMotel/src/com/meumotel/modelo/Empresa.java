package com.meumotel.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String razaoSocial;
	private String nomeFantasia;
	private Endereco endereco;
	private Localizacao localizacao;

	private List<Acomodacao> acomodacoes;
	private List<Quarto> listaQuartos = new ArrayList<Quarto>();
	private List<Suite> listaSuites = new ArrayList<Suite>();
	private List<DriveIn> listaDriveIns = new ArrayList<DriveIn>();

	private int qtdQuartosDisponiveis;
	private int qtdSuitesDisponiveis;
	private int qtdDriveInsDisponiveis;

	public Empresa() {
		acomodacoes = new ArrayList<Acomodacao>();
		endereco = new Endereco();
		localizacao = new Localizacao();
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Acomodacao> getAcomodacoes() {
		return acomodacoes;
	}

	public void setAcomodacoes(List<Acomodacao> acomodacoes) {
		this.acomodacoes = acomodacoes;
	}

	public int getQtdQuartosDisponiveis() {
		return qtdQuartosDisponiveis;
	}

	public void setQtdQuartosDisponiveis(int qtdQuartosDisponiveis) {
		this.qtdQuartosDisponiveis = qtdQuartosDisponiveis;
	}

	public int getQtdSuitesDisponiveis() {
		return qtdSuitesDisponiveis;
	}

	public void setQtdSuitesDisponiveis(int qtdSuitesDisponiveis) {
		this.qtdSuitesDisponiveis = qtdSuitesDisponiveis;
	}

	public int getQtdDriveInsDisponiveis() {
		return qtdDriveInsDisponiveis;
	}

	public void setQtdDriveInsDisponiveis(int qtdDriveInsDisponiveis) {
		this.qtdDriveInsDisponiveis = qtdDriveInsDisponiveis;
	}
	
	public void setListaQuartos(List<Quarto> listaQuartos) {
		this.listaQuartos = listaQuartos;
	}

	public void setListaSuites(List<Suite> listaSuites) {
		this.listaSuites = listaSuites;
	}

	public void setListaDriveIns(List<DriveIn> listaDriveIns) {
		this.listaDriveIns = listaDriveIns;
	}

	public List<Quarto> getListaQuartos() {
		return listaQuartos;
	}

	public List<Suite> getListaSuites() {
		return listaSuites;
	}

	public List<DriveIn> getListaDriveIns() {
		return listaDriveIns;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}
}