package com.meumotel.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.meumotel.banco.AtributosBancoDados;
import com.meumotel.modelo.Quarto;

public class QuartoDao extends PadraoDao {

	public QuartoDao(Context ctx) {
		super(ctx, AtributosBancoDados.Quarto.NOME_TABELA);
	}
	
	public List<Quarto> getListaQuarto(Integer idEmpresa) {
		List<Quarto> lista = new ArrayList<Quarto>();
		
		Quarto q = new Quarto();
		q.setId(1);
		q.setDescricao("Quarto");
		q.setNumero((short) 10);
		q.setStatus(true);

		Quarto q2 = new Quarto();
		q2.setId(1);
		q2.setDescricao("Quarto");
		q2.setNumero((short) 11);
		q2.setStatus(true);

		Quarto q3 = new Quarto();
		q3.setId(1);
		q3.setDescricao("Quarto");
		q3.setNumero((short) 12);
		q3.setStatus(true);

		Quarto q4 = new Quarto();
		q4.setId(1);
		q4.setDescricao("Quarto");
		q4.setNumero((short) 20);
		q4.setStatus(false);

		Quarto q5 = new Quarto();
		q5.setId(1);
		q5.setDescricao("Quarto");
		q5.setNumero((short) 15);
		q5.setStatus(false);

		Quarto q6 = new Quarto();
		q6.setId(1);
		q6.setDescricao("Quarto");
		q6.setNumero((short) 16);
		q6.setStatus(false);

		Quarto q7 = new Quarto();
		q7.setId(1);
		q7.setDescricao("Quarto");
		q7.setNumero((short) 18);
		q7.setStatus(true);

		lista.add(q);
		lista.add(q2);
		lista.add(q3);
		lista.add(q4);
		lista.add(q5);
		lista.add(q6);
		lista.add(q7);
		
		return lista;
	}
}
