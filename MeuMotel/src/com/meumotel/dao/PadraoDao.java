package com.meumotel.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.meumotel.banco.DbHelper;

public class PadraoDao {
	private static Context mContext;
	private static String mTable;
	
	public PadraoDao(Context ctx, String table) {
		this.mContext = ctx;
		this.mTable = table;
	}
	
	protected boolean insert(ContentValues cv) {
		SQLiteDatabase dbLocal = getDb();
		
		try {
			return dbLocal.insert(mTable, null, cv) > 0;
		} catch(Exception e) {
			Log.e(getClass().getSimpleName() + ".insert(): ", e.getMessage());
		}
		return false;
	}
	
	protected SQLiteDatabase getDb() {
		return new DbHelper(mContext).getWritableDatabase();
	}
}
