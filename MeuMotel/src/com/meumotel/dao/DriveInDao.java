package com.meumotel.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.meumotel.banco.AtributosBancoDados;
import com.meumotel.modelo.DriveIn;

public class DriveInDao extends PadraoDao {

	public DriveInDao(Context ctx) {
		super(ctx, AtributosBancoDados.DriveIn.NOME_TABELA);
	}

	public List<DriveIn> getListaDriveIn(Integer idEmpresa) {
		List<DriveIn> lista = new ArrayList<DriveIn>();
		
		DriveIn d = new DriveIn();
		d.setId(1);
		d.setDescricao("Drive-In");
		d.setNumero((short)1);
		d.setStatus(true);

		DriveIn d2 = new DriveIn();
		d2.setId(1);
		d2.setDescricao("Drive-In");
		d2.setNumero((short)2);
		d2.setStatus(true);

		DriveIn d3 = new DriveIn();
		d3.setId(1);
		d3.setDescricao("Drive-In");
		d3.setNumero((short)3);
		d3.setStatus(true);

		DriveIn d4 = new DriveIn();
		d4.setId(1);
		d4.setDescricao("Drive-In");
		d4.setNumero((short)4);
		d4.setStatus(true);

		DriveIn d5 = new DriveIn();
		d5.setId(1);
		d5.setDescricao("Drive-In");
		d5.setNumero((short)5);
		d5.setStatus(true);

		DriveIn d6 = new DriveIn();
		d6.setId(1);
		d6.setDescricao("Drive-In");
		d6.setNumero((short)6);
		d6.setStatus(true);
		
		lista.add(d6);
		lista.add(d2);
		lista.add(d3);
		lista.add(d4);
		lista.add(d5);
		lista.add(d);
		
		
		return lista;
	}
	
}
