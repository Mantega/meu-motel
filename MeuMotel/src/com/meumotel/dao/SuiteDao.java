package com.meumotel.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.meumotel.modelo.Suite;

public class SuiteDao extends PadraoDao {

	public SuiteDao(Context ctx) {
		super(ctx, "");
	}
	
	public List<Suite> getListaSuite(Integer idEmpresa) {
		List<Suite> lista = new ArrayList<Suite>();
		
		Suite s = new Suite();
		s.setId(1);
		s.setDescricao("Su�te Olimpo");
		s.setNumero((short) 1);
		s.setStatus(true);

		Suite s2 = new Suite();
		s2.setId(2);
		s2.setDescricao("Su�te Nero");
		s2.setNumero((short) 2);
		s2.setStatus(false);

		Suite s3 = new Suite();
		s3.setId(3);
		s3.setDescricao("Su�te Imperium");
		s3.setNumero((short) 3);
		s3.setStatus(true);

		Suite s4 = new Suite();
		s4.setId(4);
		s4.setDescricao("Su�te Romanus");
		s4.setNumero((short) 4);
		s4.setStatus(false);

		lista.add(s);
		lista.add(s2);
		lista.add(s3);
		lista.add(s4);
		
		return lista;
	}
}
