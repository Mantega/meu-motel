package com.meumotel.excecao;

public class HoraInvalidaException extends RuntimeException {
	private static final long serialVersionUID = -6373598607626980819L;

	public HoraInvalidaException(String message) {
		super(message);
	}
}
